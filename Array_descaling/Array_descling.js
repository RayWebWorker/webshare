Array.prototype.firstUnique = function(){
    var temp = []
    for(let i = 0; i < this.length; i++){
        //indexOf() 方法可返回某个指定的字符串值在字符串中首次出现的位置
        //indexOf() 方法对大小写敏感
        if (temp.indexOf(this[i]) === -1) {
            temp.push(this[i])
            // console.log(temp)
        }
    }
    return temp;
}

// Array.prototype.secondUnique = function(){
//     var obj = {},temp =[]
//     for (let i = 0; i < this.length; i++) {
//         // console.log(obj[this[i]])
//         //undefined 将会隐式转换为null
        
//         if( !obj[this[i]] ){
//             obj[this[i]] = true
//             temp.push(this[i])
//             // console.log(this[i])
//             // console.log(obj[this[i]])
//         }
//     }
//     return temp
// }

//基于键与值的配对
// Array.prototype.thiredUnique = function(){
//     var temp = [this[0]]
//     // console.log(temp)
//     for(let i = 1; i < this.length; i++){
//         console.log(temp)
//         if(this.indexOf(this[i]) === i){
//             temp.push(this[i])
//         }
//     }
//     return temp
// }

//效率得到了很大的提高,但是会改变原有数组的顺序
// Array.prototype.fourthUnique = function(){
//     this.sort()
//     var temp = [this[0]]
//     for (let i = 1; i < this.length; i++){
//         if( this[i] !== temp[temp.length - 1] ){
//             temp.push(this[i])
//         }
//     }
//     return temp
// }

var arr = [1,2,3,2,5,"xiao","xiao",1,3,4]

console.log(arr.firstUnique())

// console.log(arr.secondUnique())
// console.log(undefined==null)
// console.log(!null)

// console.log(arr.thiredUnique())

// console.log(arr.fourthUnique())