window.onload = function(){
    var list = document.getElementById('list');
    var prev = document.getElementById('prev');
    var next = document.getElementById('next');
    var picbox = document.getElementsByClassName('pic-box')[0];
    var button = document.getElementById('button').getElementsByTagName('span');
    var index = 1;
    var timer;
    play();
    picbox.onmouseover = stop;
    picbox.onmouseout = play;
    
    function buttonsShow(){
        for (var i = 0; i < button.length; i++) {
                if (button[i].className == 'select') {
                    button[i].className ='';
                }
            }
            button[index-1].className ='select';
    }
    function stop(){
        clearInterval(timer);
    }
    function play(){
        timer = setInterval(function(){
            prev.onclick()
        },3000)
    }
    function animlate(offset){
        var newLeft = parseInt(list.style.left)+offset;
        list.style.left = newLeft + 'px';
        if (newLeft < -2080) {
        list.style.left = -520 + 'px';   
        }
        if (newLeft >-520 ) {
        list.style.left = -2080 + 'px';   
        }
    }
    prev.onclick = function(){
        index -= 1;
            if (index < 1) {
                index = 4;
            }
            buttonsShow();
            animlate(520);
    }
    next.onclick = function(){
        index += 1;
            if (index > 4) {
                index = 1;
            }
            buttonsShow();
            animlate(-520);
    }
    for (var i = 0; i < button.length; i++) {
            (function(i) {
                button[i].onclick = function() {
                    var clickIndex = parseInt(this.getAttribute('index'));
                    var offset = 520 * (index - clickIndex); 
                    animlate(offset);
                    index = clickIndex;
                    buttonsShow();
                }
            })(i)
        }
}